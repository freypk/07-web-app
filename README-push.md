Continous Deployment - push
===========================

Legt auf der Kubernetes Zielumgebung einen SSH-Key, ohne `passphrase` an:

    ssh-keygen -C "gitlab runner"

Gebt den Privaten Key aus und legt die Ausgabe auf GitLab, unter Variables, als `File` ab und gebt dem Key den Name `SSH_KEY`.

    cat .ssh/id_rsa

Legt den URL für die Kubernetes Zielumgebung als Umgebungsvariable `KUBERNETES_CLUSTER` ab.

**Hinweis**: die Zielumgebung muss mittels **öffentlichem** DNS Zugreifbar sein.

Erweitert `.gitlab-ci.yaml` wie folgt:

    # Deploy to Kubernetes
    deploy_job:
      stage: deploy
      # Standard Image mit Linux
      image: ubuntu:latest
      script:
        - ssh -o "StrictHostKeyChecking=no" -i ${SSH_KEY} ubuntu@${KUBERNETES_CLUSTER} kubectl apply -f https://gitlab.com/<Repository>/deployment.yaml
        - ssh -o "StrictHostKeyChecking=no" -i ${SSH_KEY} ubuntu@${KUBERNETES_CLUSTER} kubectl apply -f https://gitlab.com/<Repository>/service.yaml
      needs:
        - job: release_job
      rules:
        - if: "$CI_COMMIT_TAG"    



